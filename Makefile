up:
	sudo docker-compose up -d

down:
	sudo docker-compose down --volumes

app:
	sudo docker exec -it user_online_storage_app bash

sequence-test:
	MODE=sequence go test ./... -v -bench=. -benchmem -benchtime=10s

parallel-test:
	MODE=parallel go test ./... -v -bench=. -benchmem -benchtime=10s

redis:
	sudo docker exec -it user_online_storage_redis redis-cli

redis-monitor:
	sudo docker exec -it user_online_storage_redis redis-cli monitor