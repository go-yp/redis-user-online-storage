# Redis user online storage

### Testing
```bash
make up
make sequence-test
make parallel-test
make down
```

### Result sequence
```bash
MODE=sequence go test ./... -v -bench=. -benchmem
```
```text
goos: linux
goarch: amd64
pkg: gitlab.com/go-yp/redis-user-online-storage/storage
cpu: Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz
BenchmarkSortedSetStorage
BenchmarkSortedSetStorage-12    	   15904	     75911 ns/op	     207 B/op	       6 allocs/op
BenchmarkSetStorage
BenchmarkSetStorage-12          	   16226	     72402 ns/op	     183 B/op	       5 allocs/op
BenchmarkHashStorage
BenchmarkHashStorage-12         	   16035	     69422 ns/op	     223 B/op	       7 allocs/op
PASS
ok  	gitlab.com/go-yp/redis-user-online-storage/storage	5.786s
```
```bash
MODE=parallel go test ./... -v -bench=. -benchmem
```
```text
goos: linux
goarch: amd64
pkg: gitlab.com/go-yp/redis-user-online-storage/storage
cpu: Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz
BenchmarkSortedSetStorage
BenchmarkSortedSetStorage-12    	   72022	     16541 ns/op	     209 B/op	       7 allocs/op
BenchmarkSetStorage
BenchmarkSetStorage-12          	   77587	     15094 ns/op	     185 B/op	       6 allocs/op
BenchmarkHashStorage
BenchmarkHashStorage-12         	   72858	     16064 ns/op	     225 B/op	       8 allocs/op
PASS
ok  	gitlab.com/go-yp/redis-user-online-storage/storage	4.828s
```

### Parallel with 1024 unique (ZADD vs ZADD NX), (HSET vs HSET NX)
##### ZADD, HSET
```text
BenchmarkSortedSetStorage
BenchmarkSortedSetStorage-12    	  696573	     17351 ns/op	     208 B/op	       7 allocs/op
BenchmarkSetStorage
BenchmarkSetStorage-12          	  744702	     16543 ns/op	     184 B/op	       6 allocs/op
BenchmarkHashStorage
BenchmarkHashStorage-12         	  672768	     18361 ns/op	     224 B/op	       8 allocs/op
```
##### ZADD NX, HSET NX
```text
BenchmarkSortedSetStorage
BenchmarkSortedSetStorage-12    	  687021	     17266 ns/op	     224 B/op	       7 allocs/op
BenchmarkSetStorage
BenchmarkSetStorage-12          	  760275	     16014 ns/op	     184 B/op	       6 allocs/op
BenchmarkHashStorage
BenchmarkHashStorage-12         	  671322	     17862 ns/op	     224 B/op	       8 allocs/op
```