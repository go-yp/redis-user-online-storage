package storage

import (
	"strconv"

	"github.com/go-redis/redis"
)

type HashStorage struct {
	client  *redis.Client
	keyName string
}

func NewHashStorage(client *redis.Client, keyName string) *HashStorage {
	return &HashStorage{client: client, keyName: keyName}
}

func (s *HashStorage) Store(userID int, timestamp int) error {
	return s.client.HSetNX(s.keyName, strconv.Itoa(userID), timestamp).Err()
}
