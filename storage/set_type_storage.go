package storage

import (
	"github.com/go-redis/redis"
)

type SetStorage struct {
	client  *redis.Client
	keyName string
}

func NewSetStorage(client *redis.Client, keyName string) *SetStorage {
	return &SetStorage{client: client, keyName: keyName}
}

func (s *SetStorage) Store(userID int, timestamp int) error {
	return s.client.SAdd(s.keyName, userID).Err()
}
