package storage

import (
	"github.com/go-redis/redis"
)

type SortedSetStorage struct {
	client  *redis.Client
	keyName string
}

func NewSortedSetStorage(client *redis.Client, keyName string) *SortedSetStorage {
	return &SortedSetStorage{client: client, keyName: keyName}
}

func (s *SortedSetStorage) Store(userID int, timestamp int) error {
	return s.client.ZAddNX(s.keyName, redis.Z{
		Score:  float64(timestamp),
		Member: userID,
	}).Err()
}
