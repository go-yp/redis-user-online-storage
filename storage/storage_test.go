package storage

import (
	"os"
	"sync/atomic"
	"testing"

	"github.com/stretchr/testify/require"
)

func BenchmarkSortedSetStorage(b *testing.B) {
	client, err := Client("localhost:6379")
	require.NoError(b, err)
	defer client.Close()

	client.Del("z:user:online")

	benchmarkStorage(b, NewSortedSetStorage(client, "z:user:online"))

	require.Equal(b, b.N, int(client.ZCard("z:user:online").Val()))
}

func BenchmarkSetStorage(b *testing.B) {
	client, err := Client("localhost:6379")
	require.NoError(b, err)
	defer client.Close()

	client.Del("s:user:online")

	benchmarkStorage(b, NewSetStorage(client, "s:user:online"))

	require.Equal(b, b.N, int(client.SCard("s:user:online").Val()))
}

func BenchmarkHashStorage(b *testing.B) {
	client, err := Client("localhost:6379")
	require.NoError(b, err)
	defer client.Close()

	client.Del("h:user:online")

	benchmarkStorage(b, NewHashStorage(client, "h:user:online"))

	require.Equal(b, b.N, int(client.HLen("h:user:online").Val()))
}

func benchmarkStorage(b *testing.B, storage Storage) {
	b.Helper()

	if os.Getenv("MODE") == "parallel" {
		benchmarkStorageParallel(b, storage)
	} else {
		benchmarkStorageSequence(b, storage)
	}
}

func benchmarkStorageSequence(b *testing.B, storage Storage) {
	b.Helper()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		var (
			userID    = i % 1024
			timestamp = i
		)

		err := storage.Store(userID, timestamp)

		require.NoError(b, err)
	}

	b.StopTimer()
}

func benchmarkStorageParallel(b *testing.B, storage Storage) {
	b.Helper()

	b.ResetTimer()

	var counter = uint32(0)

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var index = atomic.AddUint32(&counter, 1)

			var (
				userID    = int(index)
				timestamp = int(index)
			)

			err := storage.Store(userID, timestamp)

			require.NoError(b, err)
		}
	})

	b.StopTimer()
}
